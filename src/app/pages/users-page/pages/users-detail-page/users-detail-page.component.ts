//import { globalUsers } from './../../users-page.component';
import { ActivatedRoute } from '@angular/router';

import { Component, OnInit } from '@angular/core';
import { globalUsers } from '../../users-page.component';

@Component({
  selector: 'app-users-detail-page',
  templateUrl: './users-detail-page.component.html',
  styleUrls: ['./users-detail-page.component.scss']
})

//simulacion, en real en "ngOnInit" se haria una peticion a back que devolveria datos
export class UsersDetailPageComponent implements OnInit {

  userDetail: any = {};
//para accedoer al valor de una ruta declara en el constructor la depndencia ActivatedRoute
  constructor(private route:ActivatedRoute) { }
  //route es un objeto

  ngOnInit(): void {
    //objeto route tiene la propiedad paramMap que es un obserbable que mira cambios en la ruta
    // y al cambio ejecuta la funcion
    this.route.paramMap.subscribe(params =>{
      const userId = params.get("userId");
      //params.get("userId") devuelve el valor del identificador
      this.userDetail = globalUsers.find(users => users.id.toString()===userId);
      // users.id.toString() lo pasamos a string porque de la ruta siempre obtenemos string.
    })
  }

}
