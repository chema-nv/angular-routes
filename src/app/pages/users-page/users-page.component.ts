import { Component, OnInit } from '@angular/core';

export const globalUsers = [
 {id: 1, name: "Abel", role: "Profesor"},
 {id: 5, name: "Luis", role: "Alumno"},
 {id: 6, name: "Natalia", role: "Alumno"},
 {id: 10, name: "Sergio", role: "Alumno"},
 {id: 15, name: "Ainhoa", role: "Alumno"},
]

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {

  users = globalUsers;

  constructor() { }

  ngOnInit(): void {
    
  }

}




// import { Component, OnInit } from '@angular/core';

// export const glovalUsers = [
//   {id: 1, name:"Abel", role: "Profesor"},
//   {id: 5, name:"Luis", role: "Alumno"},
//   {id: 6, name:"Natalia", role: "Alumno"},
//   {id: 10, name:"Sergio", role: "Alumno"},
//   {id: 15, name:"AbeAinoha", role: "Alumno"},
// ]

// @Component({
//   selector: 'app-users-page',
//   templateUrl: './users-page.component.html',
//   styleUrls: ['./users-page.component.scss']
// })
// export class UsersPageComponent implements OnInit {

//   users = glovalUsers;

//   constructor() { }

//   ngOnInit(): void {
//   }

// }
