import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {

  name:string = "Chema";

  selectValue:string = "seleciona valor";

  constructor() { }

  ngOnInit(): void {
  }

}
