
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { UsersDetailPageComponent } from './pages/users-page/pages/users-detail-page/users-detail-page.component';

const routes: Routes = [
  { path: 'users', component: UsersPageComponent },

  // Los : indican que es una ruta dinamica y que tras : puede ir cuanquer cosa
  { path: 'users/:userId', component: UsersDetailPageComponent },
  
  { path: 'contact', component: ContactPageComponent},
  { path: 'about', component: AboutPageComponent},
  { path: '', component: HomePageComponent},
];  

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
