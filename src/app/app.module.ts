import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { UsersDetailPageComponent } from './pages/users-page/pages/users-detail-page/users-detail-page.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersPageComponent,
    ContactPageComponent,
    HomePageComponent,
    AboutPageComponent,
    UsersDetailPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, //necesario para usar ngmodel[()]
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
